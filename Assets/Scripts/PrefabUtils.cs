﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class PrefabUtils {
  public static IEnumerable<T> GetPrefabsWithComponent<T>() where T : class {
    string[] guids = AssetDatabase.FindAssets("t:Object", new[] {"Assets/Prefabs"});
    foreach (string guid in guids) {
      string myObjectPath = AssetDatabase.GUIDToAssetPath(guid);
      var myObjs = AssetDatabase.LoadAllAssetsAtPath(myObjectPath);

      foreach (Object thisObject in myObjs) {
        var typed = thisObject as T;
        if (!ReferenceEquals(typed, null)) {
          yield return typed;
        }
      }
    }
  }
}
#endif