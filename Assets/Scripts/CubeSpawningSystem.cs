﻿using Unity.Entities;
using Unity.Mathematics;

[UpdateInGroup(typeof(PrePhysicsGroup))]
public class CubeSpawningSystem : ComponentSystem {
  private GameInputSystem _input;
  private GameSystem _game;

  protected override void OnCreate() {
    _game = World.GetExistingSystem<GameSystem>();
    _input = World.GetExistingSystem<GameInputSystem>();
    base.OnCreate();
  }

  protected override void OnUpdate() {
    if (!_game.IsServer()) {
      return;
    }
    Entities.ForEach((Entity entity, ref NetcodeObject netObj, ref PlayerUnit playerUnit) => {
      var mouseDown = _input.GetUnitMouseDown(netObj);
      if (mouseDown) {
        _game.InstantiateObject(new NetcodeObjectInitializer {
          id = _game.GetNextNetcodeObjectId(),
          position = _input.GetUnitMousePosition(netObj),
          rotation = quaternion.identity,
          isInactive = false,
          playerId = netObj.playerId,
          prefabIndex = (int) AllNetcodePrefabs.Cube,
        });
      }
    });
  }
}
