﻿using Unity.Entities;
using Unity.Physics.Systems;

[UpdateBefore(typeof(BuildPhysicsWorld))]
public class PrePhysicsGroup : ComponentSystemGroup {}

[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class PostPhysicsGroup : ComponentSystemGroup {}
