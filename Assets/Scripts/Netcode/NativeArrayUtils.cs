﻿using System.Runtime.CompilerServices;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;


public interface INativeArraySerializable<T> where T : struct {
  bool Write(NativeArray<byte> array,
             ref int offset,
             int offsetMask = NativeArrayUtils.DefaultBufferMask);

  bool Read(NativeArray<byte> array,
            ref int offset,
            Allocator allocator,
            int offsetMask = NativeArrayUtils.DefaultBufferMask);
}

public static class NativeArrayUtils {
  public const int DefaultBufferMask = 0x0fffffff;

  public static bool Write<T>(NativeArray<byte> array,
                              ref int offset,
                              T val,
                              int offsetMask = DefaultBufferMask)
    where T : struct, INativeArraySerializable<T> {
    return val.Write(array, ref offset, offsetMask);
  }

  public static bool Read<T>(NativeArray<byte> array,
                             ref int offset,
                             out T val,
                             Allocator allocator,
                             int offsetMask = DefaultBufferMask)
    where T : struct, INativeArraySerializable<T> {
    val = default;
    return val.Read(array, ref offset, allocator, offsetMask);
  }

  public static bool Write<T>(NativeArray<byte> array,
                              ref int offset,
                              NativeArray<T> val,
                              int offsetMask = DefaultBufferMask)
    where T : struct, INativeArraySerializable<T> {
    if (!Write(array, ref offset, val.Length, offsetMask)) {
      return false;
    }

    for (int i = 0; i < val.Length; i++) {
      if (!Write(array, ref offset, val[i], offsetMask)) {
        return false;
      }
    }

    return true;
  }

  public static bool WritePrimitives<T>(NativeArray<byte> array,
                              ref int offset,
                              NativeArray<T> val,
                              int offsetMask = DefaultBufferMask)
    where T : struct {
    if (!Write(array, ref offset, val.Length, offsetMask)) {
      return false;
    }

    foreach (T subVal in val) {
      if (!WritePrimitive(array, ref offset, subVal, offsetMask)) {
        return false;
      }
    }

    return true;
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           NativeList<int> val,
                           int offsetMask = DefaultBufferMask) {
    if (!Write(array, ref offset, val.Length, offsetMask)) {
      return false;
    }

    for (int i = 0; i < val.Length; i++) {
      if (!Write(array, ref offset, val[i], offsetMask)) {
        return false;
      }
    }

    return true;
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           NativeList<bool> val,
                           int offsetMask = DefaultBufferMask) {
    if (!Write(array, ref offset, val.Length, offsetMask)) {
      return false;
    }

    for (int i = 0; i < val.Length; i++) {
      if (!Write(array, ref offset, val[i], offsetMask)) {
        return false;
      }
    }

    return true;
  }

  public static bool Read<T>(NativeArray<byte> array,
                             ref int offset,
                             out NativeArray<T> val,
                             Allocator alloc,
                             int offsetMask = DefaultBufferMask)
    where T : struct, INativeArraySerializable<T> {
    val = new NativeArray<T>();
    if (!Read(array, ref offset, out int length, offsetMask)) {
      return false;
    }

    val = new NativeArray<T>(length, alloc);
    for (int i = 0; i < val.Length; i++) {
      if (!Read(array, ref offset, out T subVal, alloc, offsetMask)) {
        return false;
      }

      val[i] = subVal;
    }

    return true;
  }

  public static bool ReadPrimitives<T>(NativeArray<byte> array,
                             ref int offset,
                             out NativeArray<T> val,
                             Allocator alloc,
                             int offsetMask = DefaultBufferMask)
    where T : struct {
    val = new NativeArray<T>();
    if (!Read(array, ref offset, out int length, offsetMask)) {
      return false;
    }

    val = new NativeArray<T>(length, alloc);
    for (int i = 0; i < val.Length; i++) {
      if (!ReadPrimitive(array, ref offset, out T subVal, offsetMask)) {
        return false;
      }

      val[i] = subVal;
    }

    return true;
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out NativeList<int> val,
                          Allocator alloc,
                          int offsetMask = DefaultBufferMask) {
    val = new NativeList<int>();
    if (!Read(array, ref offset, out int length, offsetMask)) {
      return false;
    }

    val = new NativeList<int>(length, alloc);
    for (int i = 0; i < length; i++) {
      if (!Read(array, ref offset, out int subVal, offsetMask)) {
        return false;
      }

      val.Add(subVal);
    }

    return true;
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out NativeList<bool> val,
                          Allocator alloc,
                          int offsetMask = DefaultBufferMask) {
    val = new NativeList<bool>();
    if (!Read(array, ref offset, out int length, offsetMask)) {
      return false;
    }

    val = new NativeList<bool>(length, alloc);
    for (int i = 0; i < length; i++) {
      if (!Read(array, ref offset, out bool subVal, offsetMask)) {
        return false;
      }

      val.Add(subVal);
    }

    return true;
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           float val,
                           int offsetMask = DefaultBufferMask) {
    if (array.Length < (offset & offsetMask) + sizeof(float)) {
      return false;
    }

    array.GetSubArray(offset & offsetMask, sizeof(float))
         .ReinterpretStore(0, val);
    offset += sizeof(float);
    return true;
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           bool val,
                           int offsetMask = DefaultBufferMask) {
    if (array.Length < (offset & offsetMask) + 1) {
      return false;
    }

    byte valByte = (byte) (val ? 1 : 0);
    array[offset & offsetMask] = valByte;
    offset += 1;
    return true;
  }
  
  public static bool WritePrimitive<T>(NativeArray<byte> array,
                                               ref int offset,
                                               T val,
                                               int offsetMask = DefaultBufferMask)
    where T : struct {
    int sizeOf = Unsafe.SizeOf<T>();
    if (array.Length < (offset & offsetMask) + sizeOf) {
      return false;
    }

    array.GetSubArray(offset & offsetMask, sizeOf)
         .ReinterpretStore(0, val);
    offset += sizeOf;
    return true;
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           ulong val,
                           int offsetMask = DefaultBufferMask) {
    return WritePrimitive(array, ref offset, val, offsetMask);
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           int val,
                           int offsetMask = DefaultBufferMask) {
    return WritePrimitive(array, ref offset, val, offsetMask);
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           ushort val,
                           int offsetMask = DefaultBufferMask) {
    return WritePrimitive(array, ref offset, val, offsetMask);
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           byte val,
                           int offsetMask = DefaultBufferMask) {
    return WritePrimitive(array, ref offset, val, offsetMask);
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           uint val,
                           int offsetMask = DefaultBufferMask) {
    return WritePrimitive(array, ref offset, val, offsetMask);
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           NativeString64 val,
                           int offsetMask = DefaultBufferMask) {
    return WritePrimitive(array, ref offset, val, offsetMask);
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out bool val,
                          int offsetMask = DefaultBufferMask) {
    if (array.Length < (offset & offsetMask) + 1) {
      val = false;
      return false;
    }

    val = array[offset & offsetMask] != 0;
    offset += 1;
    return true;
  }
  
  public static bool ReadPrimitive<T>(NativeArray<byte> array,
                          ref int offset,
                          out T val,
                          int offsetMask = DefaultBufferMask) where T : struct {
    int sizeOf = Unsafe.SizeOf<T>();
    if (array.Length < (offset & offsetMask) + sizeOf) {
      val = default;
      return false;
    }

    val = array.GetSubArray(offset & offsetMask, sizeOf)
               .ReinterpretLoad<T>(0);
    offset += sizeOf;
    return true;
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out float val,
                          int offsetMask = DefaultBufferMask) {
    return ReadPrimitive(array, ref offset, out val, offsetMask);
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out ulong val,
                          int offsetMask = DefaultBufferMask) {
    return ReadPrimitive(array, ref offset, out val, offsetMask);
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out int val,
                          int offsetMask = DefaultBufferMask) {
    return ReadPrimitive(array, ref offset, out val, offsetMask);
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out uint val,
                          int offsetMask = DefaultBufferMask) {
    return ReadPrimitive(array, ref offset, out val, offsetMask);
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out ushort val,
                          int offsetMask = DefaultBufferMask) {
    return ReadPrimitive(array, ref offset, out val, offsetMask);
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out byte val,
                          int offsetMask = DefaultBufferMask) {
    return ReadPrimitive(array, ref offset, out val, offsetMask);
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out NativeString64 val,
                          int offsetMask = DefaultBufferMask) {
    return ReadPrimitive(array, ref offset, out val, offsetMask);
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           int2 val,
                           int offsetMask = DefaultBufferMask) {
    if (!Write(array, ref offset, val.x, offsetMask)) {
      return false;
    }

    if (!Write(array, ref offset, val.y, offsetMask)) {
      return false;
    }

    return true;
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           float3 val,
                           int offsetMask = DefaultBufferMask) {
    if (!Write(array, ref offset, val.x, offsetMask)) {
      return false;
    }

    if (!Write(array, ref offset, val.y, offsetMask)) {
      return false;
    }

    if (!Write(array, ref offset, val.z, offsetMask)) {
      return false;
    }

    return true;
  }

  public static bool Write(NativeArray<byte> array,
                           ref int offset,
                           quaternion val,
                           int offsetMask = DefaultBufferMask) {
    if (!Write(array, ref offset, val.value.w, offsetMask)) {
      return false;
    }

    if (!Write(array, ref offset, val.value.x, offsetMask)) {
      return false;
    }

    if (!Write(array, ref offset, val.value.y, offsetMask)) {
      return false;
    }

    if (!Write(array, ref offset, val.value.z, offsetMask)) {
      return false;
    }

    return true;
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out int2 val,
                          int offsetMask = DefaultBufferMask) {
    val = int2.zero;
    if (!Read(array, ref offset, out val.x, offsetMask)) {
      return false;
    }

    if (!Read(array, ref offset, out val.y, offsetMask)) {
      return false;
    }

    return true;
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out float3 val,
                          int offsetMask = DefaultBufferMask) {
    val = float3.zero;
    if (!Read(array, ref offset, out val.x, offsetMask)) {
      return false;
    }

    if (!Read(array, ref offset, out val.y, offsetMask)) {
      return false;
    }

    if (!Read(array, ref offset, out val.z, offsetMask)) {
      return false;
    }

    return true;
  }

  public static bool Read(NativeArray<byte> array,
                          ref int offset,
                          out quaternion val,
                          int offsetMask = DefaultBufferMask) {
    val = quaternion.identity;
    if (!Read(array, ref offset, out val.value.w, offsetMask)) {
      return false;
    }

    if (!Read(array, ref offset, out val.value.x, offsetMask)) {
      return false;
    }

    if (!Read(array, ref offset, out val.value.y, offsetMask)) {
      return false;
    }

    if (!Read(array, ref offset, out val.value.z, offsetMask)) {
      return false;
    }

    return true;
  }
}
