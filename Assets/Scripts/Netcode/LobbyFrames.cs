﻿
using System;
using System.Collections.Generic;

public interface ILobbyFrame {
  void Serialize(RingBuffer buffer);
}

public class BulkFrame : ILobbyFrame {
  public byte[] bytes;
  public BulkFrame(byte[] data) {
    bytes = data;
  }

  public void Serialize(RingBuffer buffer) {
    buffer.WriteAscii($"${bytes.Length}");
    buffer.WriteCRLF();
    buffer.Write(bytes);
    buffer.WriteCRLF();
  }
}

public class SimpleFrame : ILobbyFrame {
  public readonly string value;
  
  public SimpleFrame(string val) {
    value = val;
  }

  public void Serialize(RingBuffer buffer) {
    buffer.Write('+');
    buffer.WriteAscii(value);
    buffer.WriteCRLF();
  }
}

public class ErrorFrame : ILobbyFrame {
  public readonly string value;
  
  public ErrorFrame(string val) {
    value = val;
  }

  public void Serialize(RingBuffer buffer) {
    buffer.Write('-');
    buffer.WriteAscii(value);
    buffer.WriteCRLF();
  }
}

internal class IntegerFrame : ILobbyFrame {
  public readonly int value;

  public IntegerFrame(int num) {
    value = num;
  }

  public void Serialize(RingBuffer buffer) {
    buffer.Write(':');
    buffer.WriteAscii(value.ToString());
    buffer.WriteCRLF();
  }
}

public class ArrayFrame : ILobbyFrame {
  public readonly List<ILobbyFrame> frames;

  public ArrayFrame(List<ILobbyFrame> frames) {
    this.frames = frames;
  }

  public ArrayFrame() {
    frames = new List<ILobbyFrame>();
  }

  public void Serialize(RingBuffer buffer) {
    buffer.WriteAscii($"*{frames.Count}");
    buffer.WriteCRLF();
    foreach (var frame in frames) {
      frame.Serialize(buffer);
    }
  }
}

internal struct LobbyFrame {
  public static ILobbyFrame Parse(RingBuffer buffer) {
    ILobbyFrame result = null;
    try {
      buffer.MaybeStartPeek();
      result = ParseImpl(buffer);
    } finally {
      if (result == null) {
        buffer.EndPeek();
      } else {
        buffer.CancelPeek();
      }
    }

    return result;
  }

  private static ILobbyFrame ParseImpl(RingBuffer buffer) {
    if (buffer.RemainingRead <= 0) {
      return null;
    }
    switch (buffer.ReadOne()) {
      // "+" - simple
      case 43: {
        if (buffer.ReadLine(out var str)) {
          return new SimpleFrame(str);
        }
        return null;
      }
      // "-" - error
      case 45: {
        if (buffer.ReadLine(out var str)) {
          return new ErrorFrame(str);
        }
        return null;
      }
      // "*" - array
      case 42: {
        if (!buffer.ReadLine(out var strCount)) {
          return null;
        }

        int count = int.Parse(strCount);
        var frames = new List<ILobbyFrame>(count);
        for (int i = 0; i < count; i++) {
          var frame = ParseImpl(buffer);
          if (frame == null) {
            return null;
          }

          frames.Add(frame);
        }

        return new ArrayFrame(frames);
      }
      // ":" - decimal integer
      case 58: {
        if (buffer.ReadLine(out var str)) {
          return new IntegerFrame(int.Parse(str));
        }

        return null;
      }
      // "$" - bulk
      case 36: {
        if (!buffer.ReadLine(out var strCount)) return null;
        
        int count = int.Parse(strCount);
        if (buffer.RemainingRead < count) {
          return null;
        }

        var output = new byte[count];
        if (buffer.Read(output) < count) {
          return null;
        }

        if (!buffer.ReadCRLF()) {
          return null;
        }

        return new BulkFrame(output);

      }
      default: throw new Exception("Malformed message");
    }
  }
}