﻿using Unity.Entities;

public enum CollisionSeenType {
  PersistUntilEndOfTurn,
  Debouncer
}

public struct CollisionSeen : IBufferElementData {
  public int effectId;
  public int addedOnTick;
  public CollisionSeenType seenType;
}