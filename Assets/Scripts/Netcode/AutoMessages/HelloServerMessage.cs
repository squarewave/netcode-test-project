﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct HelloServerMessage : INetcodeMessage<HelloServerMessage> {
  public NativeString64 username;

  public bool IsReliable() {
    return true;
  }

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    if (!isServer) {
      return false;
    }

    return world.GetExistingSystem<GameSystem>().InitializeNewPlayer(sendingPlayerId, this);
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public void Dispose() {}
}