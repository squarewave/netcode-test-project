﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageSerialization]
public struct PlayerLobbyStateMessage : INetcodeMessage<PlayerLobbyStateMessage> {
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public NativeArray<int> playerIds;
  public NativeArray<NativeString64> usernames;

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    return true;
  }

  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public bool IsReliable() {
    return true;
  }

  public void Dispose() {
    playerIds.Dispose();
    usernames.Dispose();
  }
}
