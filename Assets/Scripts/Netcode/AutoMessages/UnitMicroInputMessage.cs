﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateMessageSerialization]
public struct UnitMicroInputMessage : INetcodeMessage<UnitMicroInputMessage> {
  [FieldQuantization(QuantizationConstants.MinNetObjId, QuantizationConstants.MaxNetObjId)]
  public int unitId;
  [FieldQuantization("TrackedBodyState.PositionMin", "TrackedBodyState.PositionMax",
                     "TrackedBodyState.PositionBits")]
  public float3 mouseWorldPosition;
  public bool leftMouseButton;

  public bool IsReliable() {
    return false;
  }

  public bool Process(World world, int sendingPlayerId, bool isServer, ref bool skipDispose) {
    world.GetExistingSystem<GameInputSystem>().SetMicroInputState(this);
    if (isServer) {
      world.GetExistingSystem<GameSystem>().server.QueueBroadcast(this, true, sendingPlayerId);
    }
    return true;
  }
  
  public bool Serialize(NativeArray<byte> buffer, ref int offset, int bufferMask) {
    return NetcodeMessageSerialization.Serialize(this, buffer, ref offset, bufferMask);
  }

  public void Dispose() {
  }
}
