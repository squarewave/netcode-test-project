﻿using Unity.Collections;
using Unity.Entities;

[GenerateMessageFieldSerialization]
public struct PlayerData : IComponentData {
  [FieldQuantization(QuantizationConstants.MinPlayerId, QuantizationConstants.MaxPlayerId)]
  public int id;
  public NativeString64 username;
}
