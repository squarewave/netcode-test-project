﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs.LowLevel.Unsafe;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using Object = UnityEngine.Object;

[UpdateInGroup(typeof(PostPhysicsGroup))]
public class GameSystem : ComponentSystem {
  private int ticksPerStateSync = 4;
  public int jitterBufferTicks = 6;
  private int serverStateUpdateBufferTicks = 3;

  private const int MaxTrackedBodiesToSync = 30;
  public readonly Dictionary<int, float> playerPings = new Dictionary<int, float>();

  private int _mostRecentInstantiatedObjectId;
  private int _mostRecentInstantiatedTempObjectId;
  private int _trackedBodyIndex;

  public NetcodeClient client;
  public NetcodeServer server;
  public NetcodeCommon common;
  public bool started;

  private readonly ServerTickedMessageQueue<TrackedBodiesStateMessage> _remoteServerState =
    new ServerTickedMessageQueue<TrackedBodiesStateMessage>(
      ServerTickSelectionCriteria.ExactlyAtTick);

  private const float JitterBufferTicksFloatApproach = .33f;

  public int _currentClientTick;

  private float _jitterBufferTicksFloat = 6;
  private float _lastPingReceived;
  private float _lastPingSent;
  private float _lastPingRoundtrip;
  private bool _isConnected;
  private GameInputSystem _input;
  private EntityQuery _snapshotPhysicsQuery;
  private EntityQuery _temporaryObjectsQuery;
  private EntityQuery _trackedBodiesQuery;
  private EntityQuery _netcodeObjectsQuery;

  private const float PhysicsSyncSmoothingThresholdSq = 1f;

  protected override void OnCreate() {
    _input = World.GetOrCreateSystem<GameInputSystem>();
    _temporaryObjectsQuery = GetEntityQuery(typeof(TemporaryEntity));
    _snapshotPhysicsQuery = GetEntityQuery(typeof(NetcodeObject),
                                           typeof(PhysicsVelocity),
                                           typeof(Translation),
                                           typeof(Rotation),
                                           typeof(PhysicsMass),
                                           typeof(PhysicsCollider));
    _trackedBodiesQuery = GetEntityQuery(typeof(NetcodeObject),
                                         typeof(PhysicsVelocity),
                                         typeof(Translation),
                                         typeof(Rotation),
                                         typeof(PhysicsMass),
                                         typeof(PhysicsCollider),
                                         typeof(SyncTransform));
    _netcodeObjectsQuery = GetEntityQuery(typeof(NetcodeObject));
    _playersQuery = GetEntityQuery(typeof(PlayerData));
  }

  protected override void OnUpdate() {
    if (IsClient() && !_isConnected) {
      return;
    }

    _currentClientTick++;

    if (IsClient()) {
      ApplyPendingTrackedBodiesStateMessages();
      if (Time.ElapsedTime - _lastPingReceived > 1f) {
        client.QueueSendToServer(new PingMessage {
          lastPingTime = _lastPingRoundtrip,
          estimatedServerTick = ServerTickStatic.serverTick + serverStateUpdateBufferTicks
        });
        _lastPingReceived = float.MaxValue;
        _lastPingSent = (float) Time.ElapsedTime;
      }
    } else if (IsServer()) {
      if (_currentClientTick % ticksPerStateSync == 0) {
        var allocator = Allocator.Temp;

        var serverStateMessage = CollectPhysicsSnapshot(allocator);
        server.QueueBroadcast(serverStateMessage, true);

        for (int i = 0; i < serverStateMessage.trackedBodies.Length; i++) {
          serverStateMessage.trackedBodies[i] = serverStateMessage.trackedBodies[i].ApplyQuantization();
        }

        ApplyPhysicsSnapshot(serverStateMessage);
        serverStateMessage.Dispose();
      }
    }
  }

  private struct PhysicsSnapshotIndexSorter : IComparer<int> {
    public NativeArray<NetcodeObject> netcodeObjects;

    public int Compare(int x, int y) {
      return (int) netcodeObjects[y].syncScore - (int) netcodeObjects[x].syncScore;
    }
  }

  private TrackedBodiesStateMessage CollectPhysicsSnapshot(Allocator allocator) {
    var trackedBodiesCount = _snapshotPhysicsQuery.CalculateEntityCount();
    var trackedBodies =
      new NativeArray<TrackedBodyState>(math.min(trackedBodiesCount, MaxTrackedBodiesToSync),
                                        allocator);
    float3 basePosition = float3.zero;
    var result = new TrackedBodiesStateMessage {
      serverTick = ServerTickStatic.serverTick,
      trackedBodies = trackedBodies,
      basePosition = basePosition
    };

    var indices = new NativeArray<int>(trackedBodiesCount, Allocator.TempJob);
    var netObjects =
      _snapshotPhysicsQuery.ToComponentDataArray<NetcodeObject>(allocator);
    using (var velocities =
      _snapshotPhysicsQuery.ToComponentDataArray<PhysicsVelocity>(allocator))
    using (var translations =
      _snapshotPhysicsQuery.ToComponentDataArray<Translation>(allocator))
    using (var rotations = _snapshotPhysicsQuery.ToComponentDataArray<Rotation>(allocator)) {
      for (int i = 0; i < netObjects.Length; i++) {
        indices[i] = i;
        var netObj = netObjects[i];
        netObj.syncScore += 1f;
        netObj.syncScore += math.length(velocities[i].Linear);
        netObj.syncScore += math.length(velocities[i].Angular);
        netObjects[i] = netObj;
      }

      indices.Sort(new PhysicsSnapshotIndexSorter {
        netcodeObjects = netObjects,
      });

      for (int i = 0; i < trackedBodies.Length; i++) {
        var index = indices[i];
        var netObj = netObjects[index];
        netObj.syncScore = 0f;
        netObjects[index] = netObj;
        trackedBodies[i] = new TrackedBodyState {
          id = netObjects[index].id,
          position = translations[index].Value - basePosition,
          rotation = rotations[index].Value,
          velocity = velocities[index].Linear,
          angularVelocity = velocities[index].Angular,
        };
      }
    }

    _snapshotPhysicsQuery.CopyFromComponentDataArray(netObjects);
    netObjects.Dispose();
    indices.Dispose();
    return result;
  }

  private void ApplyPendingTrackedBodiesStateMessages() {
    while (_remoteServerState.GetNextMessage(out var message)) {
      ApplyPhysicsSnapshot(message);
      message.Dispose();
    }
  }

  private void ApplyPhysicsSnapshot(TrackedBodiesStateMessage message) {
    var trackedBodiesCount = _trackedBodiesQuery.CalculateEntityCount();

    var netObjects = _trackedBodiesQuery.ToComponentDataArray<NetcodeObject>(Allocator.Temp);
    var velocities = _trackedBodiesQuery.ToComponentDataArray<PhysicsVelocity>(Allocator.Temp);
    var translations = _trackedBodiesQuery.ToComponentDataArray<Translation>(Allocator.Temp);
    var rotations = _trackedBodiesQuery.ToComponentDataArray<Rotation>(Allocator.Temp);
    var masses = _trackedBodiesQuery.ToComponentDataArray<PhysicsMass>(Allocator.Temp);
    var syncs = _trackedBodiesQuery.ToComponentDataArray<SyncTransform>(Allocator.Temp);
    var idToIndex = new NativeHashMap<int, int>(trackedBodiesCount, Allocator.Temp);
    for (int i = 0; i < trackedBodiesCount; i++) {
      idToIndex.Add(netObjects[i].id, i);
    }

    foreach (TrackedBodyState msgTrackedObj in message.trackedBodies) {
      if (idToIndex.TryGetValue(msgTrackedObj.id, out var index)) {
        velocities[index] = new PhysicsVelocity
          {Angular = msgTrackedObj.angularVelocity, Linear = msgTrackedObj.velocity};

        var sync = syncs[index];
        float3 clientPosition = translations[index].Value;
        
        float3 serverPosition = msgTrackedObj.position + message.basePosition;
        if (math.distancesq(clientPosition, serverPosition) <
            PhysicsSyncSmoothingThresholdSq) {
          sync.positionOffset += clientPosition - serverPosition;
          var rotationOffset = ((Quaternion) rotations[index].Value).eulerAngles -
                               ((Quaternion) msgTrackedObj.rotation).eulerAngles;
          // sync.rotationOffset += (float3) rotationOffset;
        } else {
          sync = default;
        }

        syncs[index] = sync;

        translations[index] = new Translation {Value = serverPosition};
        rotations[index] = new Rotation {Value = msgTrackedObj.rotation};
        var oldMass = masses[index];
        masses[index] = oldMass;
      }
    }

    _trackedBodiesQuery.CopyFromComponentDataArray(velocities);
    _trackedBodiesQuery.CopyFromComponentDataArray(translations);
    _trackedBodiesQuery.CopyFromComponentDataArray(rotations);
    _trackedBodiesQuery.CopyFromComponentDataArray(masses);
    _trackedBodiesQuery.CopyFromComponentDataArray(syncs);

    netObjects.Dispose();
    velocities.Dispose();
    translations.Dispose();
    rotations.Dispose();
    masses.Dispose();
    idToIndex.Dispose();
  }

  public void QueueForDirectOrIndirectBroadcast<T>(T message) where T : INetcodeMessage<T> {
    if (IsServer()) {
      server.QueueBroadcast(message, true);
    } else if (IsClient()) {
      client.QueueSendToServer(message);
    }
  }

  public bool IsClient() {
    return client != null;
  }

  public bool IsServer() {
    return server != null;
  }

  public void StartServer() {
    if (server != null) {
      return;
    }

    Debug.Log($"Starting server");
    server = new NetcodeServer(World);
    common = server;
    server.Start();
    started = true;
  }

  private int indexThing = 0;
  private static readonly int ShadowColor = Shader.PropertyToID("_ShadowColor");
  private static readonly int HighlightColor = Shader.PropertyToID("_HighlightColor");
  private EntityQuery _playersQuery;
  private bool _turnsInitialized;
  private bool _gameStarted;

  public void StartClient(IntPtr hostPeer) {
    if (client != null) {
      return;
    }

    client = new NetcodeClient(World);
    common = client;
    client.Start(hostPeer);
    started = true;
  }

  public bool InitializeNewPlayer(int playerId, HelloServerMessage message) {
    if (_gameStarted) {
      return false;
    }
    
    AddPlayerDataEntity(message.username, playerId);
      
    InstantiateObject(new NetcodeObjectInitializer {
      id = GetNextNetcodeObjectId(),
      position = new float3(0, 3f, 0),
      rotation = quaternion.identity,
      isInactive = false,
      playerId = playerId,
      prefabIndex = (int) AllNetcodePrefabs.Player,
      isDeathPrefab = false,
    });

    var netcodeObjQuery =
      GetEntityQuery(typeof(NetcodeObject), typeof(Translation), typeof(Rotation));
    var netcodeObjCount = netcodeObjQuery.CalculateEntityCount();
    var netcodeObjectInitializers =
      new NativeArray<NetcodeObjectInitializer>(netcodeObjCount, Allocator.Temp);
    var entities = netcodeObjQuery.ToEntityArray(Allocator.TempJob);
    var netcodeObjects = netcodeObjQuery.ToComponentDataArray<NetcodeObject>(Allocator.Temp);
    var rotations = netcodeObjQuery.ToComponentDataArray<Rotation>(Allocator.Temp);
    var translations = netcodeObjQuery.ToComponentDataArray<Translation>(Allocator.Temp);
    for (int i = 0; i < netcodeObjCount; i++) {
      var netcodeObj = netcodeObjects[i];
      netcodeObjectInitializers[i] = new NetcodeObjectInitializer {
        id = netcodeObj.id,
        playerId = netcodeObj.playerId,
        prefabIndex = netcodeObj.prefabIndex,
        rotation = rotations[i].Value,
        position = translations[i].Value,
        isInactive = false
      };
    }

    var players = _playersQuery.ToComponentDataArray<PlayerData>(Allocator.Temp);
    var childIds = new NativeList<int>(Allocator.Temp);
    server.QueueClientMessage(playerId, new HelloClientMessage {
      playerId = playerId,
      players = players,
      netcodeObjectInitializers = netcodeObjectInitializers,
      childIds = childIds
    });
    players.Dispose();

    server.QueueBroadcast(new PlayerEnteredMessage {
      playerId = playerId,
      username = message.username,
    }, true, playerId);

    entities.Dispose();
    netcodeObjectInitializers.Dispose();
    netcodeObjects.Dispose();
    childIds.Dispose();
    return true;
  }

  public int GetNextNetcodeObjectId() {
    return IsServer() ? ++_mostRecentInstantiatedObjectId : --_mostRecentInstantiatedTempObjectId;
  }

  public bool InitializeClient(HelloClientMessage message) {
    var input = _input;
    if (input.playerId != NetcodeObject.UnassignedPlayerId) {
      Debug.LogError("Initializing client twice");
      return false;
    }

    _isConnected = true;
    input.playerId = message.playerId;

    foreach (var playerData in message.players) {
      AddPlayerDataEntity(playerData.username, playerData.id);
    }

    var childIdsIndex = 0;
    foreach (var initializer in message.netcodeObjectInitializers) {
      if (initializer.prefabIndex == NetcodeObject.UnassignedPrefabIndex) {
        continue;
      }

      InstantiatePrefab(initializer.prefabIndex, initializer.playerId, initializer.id,
                                     initializer.isDeathPrefab, initializer.isInactive,
                                     initializer.position, initializer.rotation,
                                     ref message.childIds, ref childIdsIndex);
    }

    return true;
  }

  public void ProcessTrackedBodiesStateMessage(TrackedBodiesStateMessage message,
                                               ref bool skipDispose) {
    if (!IsClient()) {
      return;
    }

    Debug.DrawLine(Vector3.zero, Vector3.up + Vector3.forward, Color.black);

    if (message.serverTick < ServerTickStatic.serverTick) {
      return;
    }

    _remoteServerState.QueueMessage(message.serverTick, message);
    skipDispose = true;
  }

  public int GetJitterBufferedServerTick() {
    return ServerTickStatic.serverTick + (IsServer() ? 0 : jitterBufferTicks * 2);
  }

  public int GetNextJitterBufferedServerTick() {
    return GetJitterBufferedServerTick() + 1;
  }

  public Entity InstantiateObject(NetcodeObjectInitializer initializer) {
    var playerId = initializer.playerId;
    var objectId = initializer.id;
    var prefabIndex = initializer.prefabIndex;
    var isDeathPrefab = initializer.isDeathPrefab;
    var translation = initializer.position;
    var rotation = initializer.rotation;
    var isInactive = initializer.isInactive;

    var childIdsIndex = 0;
    var childIds = new NativeArray<int>(32, Allocator.TempJob);
    Entity result = InstantiatePrefab(prefabIndex, playerId, objectId, isDeathPrefab,
                                      isInactive, translation, rotation,
                                      ref childIds, ref childIdsIndex);
    if (IsServer()) {
      if (IsTempObject(objectId)) {
        Debug.LogError("Trying to instantiate a temp object on server");
      }

      server.QueueBroadcast(new ObjectInstantiatedMessage {
        initializer = initializer,
        childIds = childIds
      }, true);
    } else {
      if (!IsTempObject(objectId)) {
        Debug.LogError("Trying to instantiate a non-temp object on client");
      }

      EntityManager.AddComponentData(result, new TemporaryEntity());
    }

    childIds.Dispose();

    return result;
  }

  public void PurgeTempEntities() {
    var tempEntities = _temporaryObjectsQuery.ToEntityArray(Allocator.TempJob);
    foreach (var entity in tempEntities) {
      if (EntityManager.HasComponent<GameObject>(entity)) {
        var obj = EntityManager.GetComponentObject<GameObject>(entity);
        Object.Destroy(obj);
      }

      EntityManager.DestroyEntity(entity);
    }

    tempEntities.Dispose();
  }

  private bool IsTempObject(int objectId) {
    return objectId < 0;
  }

  public Entity InstantiateObject(NetcodeObjectInitializer initializer, NativeArray<int> childIds) {
    var playerId = initializer.playerId;
    var objectId = initializer.id;
    var prefabIndex = initializer.prefabIndex;
    var translation = initializer.position;
    var rotation = initializer.rotation;
    var isDeathPrefab = initializer.isDeathPrefab;
    var isInactive = initializer.isInactive;

    var childIdsIndex = 0;
    return InstantiatePrefab(prefabIndex, playerId, objectId, isDeathPrefab,
                             isInactive,
                             translation, rotation,
                             ref childIds, ref childIdsIndex);
  }

  private Entity InstantiatePrefab(int prefabIndex,
                                   int playerId,
                                   int objectId,
                                   bool isDeathPrefab,
                                   bool isInactive,
                                   float3 translation,
                                   quaternion rotation,
                                   ref NativeArray<int> childIds,
                                   ref int childNetcodeIdsIndex) {
    if (prefabIndex == NetcodeObject.UnassignedPrefabIndex) {
      Debug.LogError("Trying to instantiate unassigned prefab");
      return Entity.Null;
    }

    var input = _input;

    var prefabList = isDeathPrefab
      ? GetSingleton<NetcodePrefabList>().deathPrefabs
      : GetSingleton<NetcodePrefabList>().prefabs;
    var prefabObjList = isDeathPrefab
      ? NetcodePrefabListSingleton.instance.deathPrefabs
      : NetcodePrefabListSingleton.instance.prefabs;
    var prefab = EntityManager.GetBuffer<NetcodePrefabBuffer>(prefabList)[prefabIndex];
    var entity = EntityManager.Instantiate(prefab.value);

    // Set up the netcode object - we have to assume that this exists, as its the only way to track
    // instantiated netcode prefabs.
    var netcodeObject = new NetcodeObject {
      playerId = playerId, id = objectId, prefabIndex = prefabIndex, isDeathPrefab = true
    };
    EntityManager.AddComponentData(entity, netcodeObject);
    EntityManager.SetComponentData(entity, new Translation {
      Value = translation
    });
    EntityManager.SetComponentData(entity, new Rotation {
      Value = rotation
    });

    // Netcode objects also must contain an associated GameObject. We don't want to use the hybrid
    // renderer because it's currently garbage, so we need to keep GameObjects in sync for all
    // visible entities. Here we just assume that anything we're instantiating across the network
    // is visible.
    var entityObj = Object.Instantiate(prefabObjList[prefabIndex]);
    EntityManager.AddComponentObject(entity, entityObj);

    if (EntityManager.HasComponent<LinkedEntityGroup>(entity)) {
      var childrenBuf = EntityManager.GetBuffer<LinkedEntityGroup>(entity);
      var childrenArray = new NativeArray<Entity>(childrenBuf.Length, Allocator.TempJob);
      for (int i = 0; i < childrenBuf.Length; i++) {
        childrenArray[i] = childrenBuf[i].Value;
      }

      var childEntityIndex = 0;
      var childObjIndex = 0;
      for (int i = 1; i < childrenArray.Length; i++) {
        var child = childrenArray[i];
        if (!EntityManager.HasComponent<Parent>(child) ||
            EntityManager.GetComponentData<Parent>(child).Value == prefab.value) {
          var gameObject = entityObj.transform.GetChild(childObjIndex++).gameObject;
          EntityManager.AddComponentObject(child, gameObject);
        }

        EntityManager.SetComponentData(child, new Translation {
          Value = translation
        });
        EntityManager.SetComponentData(child, new Rotation {
          Value = rotation
        });

        // Only sync physics children, on the assumption that they are the only objects
        // which will meaningfully diverge from their parents and affect the game state.
        if (EntityManager.HasComponent<PhysicsVelocity>(child)) {
          if (IsServer()) {
            var childNetObj = new NetcodeObject {
              playerId = playerId,
              id = GetNextNetcodeObjectId(),
              prefabIndex = NetcodeObject.UnassignedPrefabIndex,
              isDeathPrefab = true
            };
            EntityManager.AddComponentData(child, childNetObj);
            childIds[childEntityIndex++] = childNetObj.id;
          } else {
            var childNetObj = new NetcodeObject {
              playerId = playerId,
              id = IsTempObject(objectId)
                ? GetNextNetcodeObjectId()
                : childIds[childNetcodeIdsIndex++],
              prefabIndex = NetcodeObject.UnassignedPrefabIndex,
              isDeathPrefab = true
            };
            EntityManager.AddComponentData(child, childNetObj);

            if (IsTempObject(objectId)) {
              EntityManager.AddComponentData(child, new TemporaryEntity());
            }
          }
        }
      }

      childrenArray.Dispose();
    }

    return entity;
  }

  public void ProcessPingMessage(int playerId, PingMessage message) {
    if (IsClient()) {
      _lastPingRoundtrip = (float) (Time.ElapsedTime - _lastPingSent);
      _lastPingReceived = (float) Time.ElapsedTime;

      var deltaTick = message.estimatedServerTick;
      _jitterBufferTicksFloat += (deltaTick - _jitterBufferTicksFloat) *
                                 JitterBufferTicksFloatApproach;
      jitterBufferTicks = Mathf.CeilToInt(_jitterBufferTicksFloat) + 1;
    } else if (IsServer()) {
      playerPings[playerId] = message.lastPingTime;
      message.estimatedServerTick = ServerTickStatic.serverTick - message.estimatedServerTick;
      server.QueueClientMessage(playerId, message);
    }
  }

  public void DestroyObjects(NativeArray<int> objectIds) {
    if (objectIds.Length == 0) {
      return;
    }

    var netcodeObjectsCount = _netcodeObjectsQuery.CalculateEntityCount();
    var entities = _netcodeObjectsQuery.ToEntityArray(Allocator.TempJob);
    var netObjects = _netcodeObjectsQuery.ToComponentDataArray<NetcodeObject>(Allocator.TempJob);
    var idToIndex = new NativeHashMap<int, int>(netcodeObjectsCount, Allocator.TempJob);
    for (int i = 0; i < netcodeObjectsCount; i++) {
      idToIndex.Add(netObjects[i].id, i);
    }

    foreach (int id in objectIds) {
      if (idToIndex.ContainsKey(id)) {
        var entity = entities[idToIndex[id]];
        DestroyEntity(entity);
        idToIndex.Remove(id);
      }
    }

    if (IsServer()) {
      server.QueueBroadcast(new ObjectsDestroyedMessage {
        objectIds = objectIds
      }, true);
    }

    netObjects.Dispose();
    idToIndex.Dispose();
    entities.Dispose();
  }

  public void DestroyEntity(Entity entity, int netObjId) {
    DestroyEntity(entity);

    if (IsServer()) {
      var objectIds = new NativeList<int>(Allocator.TempJob) {netObjId};
      server.QueueBroadcast(new ObjectsDestroyedMessage {
        objectIds = objectIds
      }, true);
      objectIds.Dispose();
    }
  }

  private void DestroyEntity(Entity entity) {
    var gameObj = EntityManager.GetComponentObject<GameObject>(entity);
    if (EntityManager.HasComponent<DetachChildrenOnDestroy>(entity)) {
      gameObj.transform.DetachChildren();
    }

    Object.Destroy(gameObj);
    EntityManager.DestroyEntity(entity);
  }

  public void Start(NativeString64 username) {
    if (!IsClient() && !IsServer()) {
      StartServer();
    }

    if (IsServer()) {
      var input = _input;
      input.playerId = input.GetNewPlayerId();
      if (input.playerId != NetcodeCommon.ServerPlayerId) {
        Debug.LogError("Bad Server Player ID created");
      }

      AddPlayerDataEntity(username, input.playerId);
      
      InstantiateObject(new NetcodeObjectInitializer {
        id = GetNextNetcodeObjectId(),
        position = new float3(0, 3f, 0),
        rotation = quaternion.identity,
        isInactive = false,
        playerId = input.playerId,
        prefabIndex = (int) AllNetcodePrefabs.Player,
        isDeathPrefab = false,
      });
    } else {
      var hello = new HelloServerMessage {
        username = username
      };
      client.QueueSendToServer(hello);
    }
  }

  public void AddPlayerDataEntity(NativeString64 username, int playerId) {
    var playerEntity = EntityManager.CreateEntity();
    EntityManager.AddComponentData(playerEntity, new PlayerData {
      id = playerId,
      username = username
    });
  }

  public Entity GetPlayerEntity() {
    var playerEntities = _playersQuery.ToEntityArray(Allocator.TempJob);
    var players = _playersQuery.ToComponentDataArray<PlayerData>(Allocator.TempJob);

    var result = Entity.Null;
    for (int i = 0; i < players.Length; i++) {
      if (players[i].id == _input.playerId) {
        result = playerEntities[i];
        break;
      }
    }

    players.Dispose();
    playerEntities.Dispose();

    return result;
  }
}
