﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

public class LobbyWindow : MonoBehaviour {
  public RectTransform playernamePrefab;
  public RectTransform playernamesRegion;
  public GameObject enableOnSuccess;
  public GameObject usernameOverlay;
  public TMP_InputField roomCode;

  public float nextPlayernameY;
  public float playernameMarginY;

  private GameSystem _game;
  private GameInputSystem _input;

  private EntityQuery _playersQuery;

  private readonly Dictionary<int, RectTransform> _playerIdToPlayernameCard =
    new Dictionary<int, RectTransform>();

  private World _world;
  private Transform _startButton;
  private bool _isServer;

  private void Start() {
    usernameOverlay.SetActive(true);
    usernameOverlay.transform.Find("UsernameInput").GetComponent<TMP_InputField>().Select();
    _world = World.DefaultGameObjectInjectionWorld;
    _game = _world.GetOrCreateSystem<GameSystem>();
    _input = _world.GetOrCreateSystem<GameInputSystem>();
    _playersQuery =
      _world.EntityManager.CreateEntityQuery(typeof(PlayerData));
    _game.common.AddPersistentListener<PlayerLobbyStateMessage>(OnPlayerLobbyState);
    roomCode.text = NetcodeLobbySystem.instance.activeRoomId;
    IceProtocolManager.instance.OnError += OnError;
    StartCoroutine(PollAndSendLobbyState());
    _startButton = transform.Find("StartButton");
    _startButton.gameObject.SetActive(false);
  }

  private void OnError(int obj) {
  }

  public void OnRoomCodeChange() {
    roomCode.text = NetcodeLobbySystem.instance.activeRoomId;
  }

  public void SetUsername(string username) {
    usernameOverlay.SetActive(false);
    _game.Start(username);
    _startButton.gameObject.SetActive(_game.IsServer()); 
  }

  IEnumerator PollAndSendLobbyState() {
    var waitOneHalfSecond = new WaitForSeconds(.5f);
    while (true) {
      var players = _playersQuery.ToComponentDataArray<PlayerData>(Allocator.Temp);

      if (_game.IsServer()) {
        var playerLobbyState = new PlayerLobbyStateMessage {
          playerIds = new NativeArray<int>(players.Length, Allocator.Temp),
          usernames = new NativeArray<NativeString64>(players.Length, Allocator.Temp),
        };

        for (int i = 0; i < players.Length; i++) {
          playerLobbyState.playerIds[i] = players[i].id;
          playerLobbyState.usernames[i] = players[i].username;
        }

        _game.server.QueueBroadcast(playerLobbyState, false);
        playerLobbyState.Dispose();
      }

      players.Dispose();

      yield return waitOneHalfSecond;
    }
  }

  private void OnPlayerLobbyState(PlayerLobbyStateMessage message) {
    for (int i = 0; i < message.playerIds.Length; i++) {
      var playerId = message.playerIds[i];
      var username = message.usernames[i];

      AddOrUpdatePlayerCard(playerId, username.ToString());
    }
  }

  private void OnDestroy() {
    _game.common.RemovePersistentListener<PlayerLobbyStateMessage>(OnPlayerLobbyState);
  }

  private void AddOrUpdatePlayerCard(int playerId, string username) {
    if (!_playerIdToPlayernameCard.TryGetValue(playerId, out var card)) {
      card = Instantiate(playernamePrefab, playernamesRegion);
      var pos = card.anchoredPosition;
      pos.y = nextPlayernameY;
      card.anchoredPosition = pos;
      card.GetChild(1).GetComponent<TextMeshProUGUI>().SetText(username);
      nextPlayernameY -= playernamePrefab.rect.height + playernameMarginY;
      _playerIdToPlayernameCard[playerId] = card;
    }
  }

  public void Ready() {
    if (enableOnSuccess != null) {
      enableOnSuccess.SetActive(true);
    }
    Destroy(gameObject);
  }
}
