﻿using System;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

public class InputSingleton : MonoBehaviour {
  public static InputSingleton instance;
  private readonly HashSet<GameObject> _hoveredUiElements = new HashSet<GameObject>();
  private bool _seenFixedUpdate = false;
  private Camera _camera;
  private EntityQuery _entityQuery;
  private GameInputSystem _input;
  private Entity _playerUnit;
  private World _world;
  private int _selectedSecondaryAbilityIndex = 1;
  private GameSystem _game;

  private void Start() {
    instance = this;
    _camera = Camera.main;
    _world = World.DefaultGameObjectInjectionWorld;
    _input = _world.GetOrCreateSystem<GameInputSystem>();
    _game = _world.GetOrCreateSystem<GameSystem>();
    _entityQuery =
      _world.EntityManager.CreateEntityQuery(
        typeof(NetcodeObject), typeof(PlayerUnit), typeof(Translation));
  }

  private void FixedUpdate() {
    _seenFixedUpdate = true;
  }

  private void Update() {
    using var entities = _entityQuery.ToEntityArray(Allocator.TempJob);
    using var netObjs = _entityQuery.ToComponentDataArray<NetcodeObject>(Allocator.TempJob);
    if (_playerUnit == Entity.Null) {
      for (int i = 0; i < entities.Length; i++) {
        if (netObjs[i].playerId == _input.playerId) {
          _playerUnit = entities[i];
        }
      } 
    }

    _hoveredUiElements.RemoveWhere(o => o == null);

    if (_playerUnit != Entity.Null) {
      var microInput = new UnitMicroInputMessage {
        unitId = _world.EntityManager.GetComponentData<NetcodeObject>(_playerUnit).id,
        mouseWorldPosition = GetUnitMousePosition(),
        leftMouseButton = GetUnitLeftMouseButton(),
      };
      _game.QueueForDirectOrIndirectBroadcast(microInput);
    }
    
    _seenFixedUpdate = false;
  }

  public Vector3 GetUnitMousePosition() {
    Vector3 mousePositionWorld = Vector3.zero;
    Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
    var plane = new Plane(Vector3.back, Vector3.zero);
    if (plane.Raycast(ray, out var enter)) {
      mousePositionWorld = ray.GetPoint(enter);
    }

    return mousePositionWorld;
  }

  public bool GetUnitLeftMouseButton() {
    return Input.GetMouseButton(0);
  }

  public bool SpacebarPressed() {
    // NOTE: this is not good enough
    return Input.GetKeyDown(KeyCode.Space);
  }
}
